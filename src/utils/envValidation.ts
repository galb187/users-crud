import { cleanEnv, str, email, json, num } from 'envalid'

export const environment = cleanEnv(process.env, {
    NODE_ENV: str({ choices: ['development', 'test', 'production', 'staging']}),
    HOST: str({default:'localhost'}),
    PORT: num({default: 3030})
})