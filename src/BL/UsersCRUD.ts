import { IUser } from '../typings/typings.js';
import fs from 'fs/promises'
import { v4 as uuidv4 } from 'uuid';
import { ResponseMessage } from '../routes/users.js';
import { UrlNotFoundException } from '../exceptions/UrlNotFoundException.js';
import { writeJsonFile } from '../DAL/fileRW.js';
import { DB_FILE_PATH } from "../index.js";


export async function getUser(id:string, arr : IUser[], url :string) {
    const user = arr.filter((item) => item.user_id === id)[0];
    if (user) {
        const responseMsg: ResponseMessage = {
          status: 200,
          message: "User found",
          data: user,
        };
        return responseMsg
      } else {
        throw new UrlNotFoundException("user does not exist in database", url)
      }
}

export async function addUser(usernameToAdd : IUser, users: IUser[], url :string) {
    //find it username exist
    const validUsername = users.find((item) => item.username === usernameToAdd.username);
    if (!validUsername) {
        const userToAdd : IUser = {
            user_id: uuidv4(),
            username : usernameToAdd.username,
            age : usernameToAdd.age
         }
      const updatedData = [...users,userToAdd]
      await writeJsonFile(DB_FILE_PATH, updatedData)
      const responseMsg: ResponseMessage = {
        status: 200,
        message: "User was added",
        data: updatedData,
      };
      return responseMsg
    } else
      throw new UrlNotFoundException("username is already exist in database",url);
}

export async function deleteUser(userIdDelete:string, users: IUser[], url : string) {
        const validUser = users.find(
          (item) => item.user_id === userIdDelete);
          if (validUser) {
            const updatedData = users.filter(item => item.user_id !== userIdDelete)
            await writeJsonFile(DB_FILE_PATH, updatedData);
            const responseMsg: ResponseMessage = {
            status: 200,
            message: "User was deleted",
            data: updatedData,
            };
            return responseMsg
        }
            else
            throw new UrlNotFoundException("user id does not exist in database",url);
}

export async function updateUser(user : IUser, users: IUser[], url :string) {
    const userIndexToUpdate = users.findIndex(u => u.user_id === user.user_id) 
      if (userIndexToUpdate >= 0) {
        users[userIndexToUpdate] = {...users[userIndexToUpdate], ...user}
        await writeJsonFile(DB_FILE_PATH, users);
        const responseMsg: ResponseMessage = {
            status: 200,
            message: "User was updated",
            data: users,
        };
        return responseMsg
    }
    else 
        throw new UrlNotFoundException("user id does not exist in database",url)    
}