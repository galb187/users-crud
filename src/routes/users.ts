import { NextFunction, Request, Response, Router } from "express";
import log from "@ajar/marker";
import { getUser, addUser, deleteUser, updateUser } from "../BL/UsersCRUD.js";
import { IUser } from "../typings/typings.js";
import { readJsonFile, writeJsonFile } from "../DAL/fileRW.js";
import { DB_FILE_PATH } from "../index.js";
import { UrlNotFoundException } from "../exceptions/UrlNotFoundException.js";

const router = Router();


export interface ResponseMessage {
  status: number;
  message: string;
  data: any;
}

// reading file mw
router.use(async (req: Request, res: Response, next: NextFunction) => {
  try {
    req.users = await readJsonFile(DB_FILE_PATH);
    next();
  } catch (err) {
    next(err);
  }
});

// get user by ID
router.get(
  "/:user_id",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { user_id } = req.params;
      const resToDeliver = await getUser(user_id, req.users, req.originalUrl);
      res.status(resToDeliver.status).json(resToDeliver);
    } catch (err) {
      log.red(err);
      next(err);
    }
  }
);

// add new user to DB
router.post(
  "/newUser",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const allUsers: IUser[] = req.users;
      const usernameToAdd = (req.body as IUser);
      const resToDeliver = await addUser(usernameToAdd, req.users, req.originalUrl);
      res.status(resToDeliver.status).json(resToDeliver);
    } catch (err) {
        next(err)
    }
  }
);

// delete user from DB
router.delete(
  "/deleteUser/:idToDelete",
  async (req: Request, res: Response, next: NextFunction) => {
    try{
        const allUsers: IUser[] = req.users;
        const delete_user_id: string = req.params.idToDelete;
        const resToDeliver = await deleteUser(delete_user_id, allUsers, req.originalUrl);
        res.status(resToDeliver.status).json(resToDeliver);
    }catch(err){
        next(err)
    }   
  }
);

// update user in DB
router.put("/updateUser", async (req: Request, res: Response, next: NextFunction) => {
    try{
        const allUsers: IUser[] = req.users;
        const updatedUser = req.body as IUser;
        const resToDeliver = await updateUser(updatedUser, allUsers, req.originalUrl);
        res.status(resToDeliver.status).json(resToDeliver);
    }catch(err){
        next(err)
    }
  }
);

export default router;
