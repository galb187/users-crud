import express, { NextFunction, Request, Response, Router } from 'express';
import log from '@ajar/marker';
import morgan from 'morgan';
import usersRouter from './routes/users.js'
import fs from 'fs/promises'
import { catchErrorAndRespondMW, logErrorsMW, urlNotFoundMW } from './middlewares/exception.js';
import { logHttpRequestMW } from './middlewares/loggers.js';
import { addIDToRequest } from './middlewares/requestAdjustments.js';
import { environment } from './utils/envValidation.js';

const app = express()

export const LOG_FILE_PATH = process.cwd()+'/src/users.log'
export const ERROR_LOG_FILE_PATH = process.cwd()+'/src/error_log_file.log'
export const DB_FILE_PATH = process.cwd()+'/src/data/users.json'

//add id to each request
app.use(addIDToRequest)
// gloval middlewares
app.use(express.json()) //relevant for req.body
// Log Requests
app.use( morgan('dev') );
app.use(logHttpRequestMW(LOG_FILE_PATH))

//Routing
app.use('/users',usersRouter)

//Catch & Log Errors
app.use(urlNotFoundMW)
app.use(logErrorsMW(ERROR_LOG_FILE_PATH))
app.use(catchErrorAndRespondMW)

//Server UP
app.listen(environment.PORT, environment.HOST,  ()=> {
    log.magenta(`🌎  listening on`,`http://${environment.HOST}:${environment.PORT}`);
});

