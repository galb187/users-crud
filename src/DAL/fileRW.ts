import fs from 'fs/promises'
import { IUser } from '../typings/typings.js';

export async function readJsonFile(path:string) {
    const fileDataStr = await fs.readFile(path, 'utf-8');
    const fileUserData : IUser[] = JSON.parse(fileDataStr)
    return fileUserData
}


export async function writeJsonFile(path:string, updatedData: IUser[]) {
    await fs.writeFile(path,JSON.stringify(updatedData));
    return 'updated'
}

