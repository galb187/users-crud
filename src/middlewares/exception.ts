import { ServerException } from "../exceptions/ServerException.js";
import { UrlNotFoundException } from "../exceptions/UrlNotFoundException.js";
import { HttpException } from "../exceptions/httpException.js";
import {Request, NextFunction, Response} from 'express';
import fs from 'fs'


export async function urlNotFoundMW(req:Request, res:Response, next:NextFunction) {
    next(new UrlNotFoundException(`URL: ${req.url} was not found`,req.url))
}

interface ErrorResponse{
    statusCode:number,
    message:string,
    url : string,
    stack?:string
}

export async function catchErrorAndRespondMW(err:HttpException,req:Request, res:Response, next:NextFunction) {
    const errorToSend : ErrorResponse = {
        statusCode: err.statusCode || 500,
        message : err.message || 'Something went wrong',
        url: req.url,
        stack : err.stack
    }
    res.status(err.statusCode).json(errorToSend)
}

export function logErrorsMW(path:string){
    const streamer = fs.createWriteStream(path,{ flags: 'a' });
    return (error:HttpException, req: Request, res: Response, next: NextFunction) =>{
        streamer.write(`${req.request_id} :: ${error.statusCode} :: ${error.message} :: ${Date.now()} >> ${error.stack} \n`);
        next(error)
    }
}
