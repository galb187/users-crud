export class HttpException extends Error{
    constructor(public message : string, public statusCode:number, 
        public originURL : string ){
        super(message)
    }
}