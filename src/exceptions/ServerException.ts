import { HttpException } from "./HttpException.js";

export class ServerException extends HttpException{
    constructor(public message : string, public originUrl:string,
         public statusCode:number = 500){
        super(message,statusCode,originUrl)
    }
}