import { HttpException } from "./HttpException.js";

export class UrlNotFoundException extends HttpException{
    constructor(public message : string, public originalUrl : string,
         public statusCode:number = 404){
        super(message,statusCode,originalUrl)
    }
}