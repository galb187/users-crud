export declare interface IUser{
    user_id : string,
    username : string,
    age : number
}

declare global{
    namespace Express{
        interface Request{
            users : IUser[],
            request_id : string
        }
    }
}